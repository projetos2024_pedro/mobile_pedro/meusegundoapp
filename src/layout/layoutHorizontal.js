import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, ScrollView } from "react-native";

export default function LayoutHorizontal() {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <StatusBar style="auto" />
        <ScrollView horizontal={true}>
          <View style={styles.box2}></View>
          <View style={styles.box3}></View>
          <View style={styles.box1}></View>
          <View style={styles.box4}></View>
          <View style={styles.box5}></View>
          <View style={styles.box2}></View>
          <View style={styles.box3}></View>
          <View style={styles.box1}></View>
          <View style={styles.box4}></View>
          <View style={styles.box5}></View>
          <View style={styles.box2}></View>
          <View style={styles.box3}></View>
          <View style={styles.box1}></View>
          <View style={styles.box4}></View>
          <View style={styles.box5}></View>
          <View style={styles.box2}></View>
          <View style={styles.box3}></View>
          <View style={styles.box1}></View>
          <View style={styles.box4}></View>
          <View style={styles.box5}></View>
          <View style={styles.box6}></View>
          <View style={styles.box7}></View>
          <View style={styles.box8}></View>
          <View style={styles.box9}></View>
          <View style={styles.box10}></View>
          <View style={styles.box6}></View>
          <View style={styles.box7}></View>
          <View style={styles.box8}></View>
          <View style={styles.box9}></View>
          <View style={styles.box10}></View>
          <View style={styles.box6}></View>
          <View style={styles.box7}></View>
          <View style={styles.box8}></View>
          <View style={styles.box9}></View>
          <View style={styles.box10}></View>
          <View style={styles.box6}></View>
          <View style={styles.box7}></View>
          <View style={styles.box8}></View>
          <View style={styles.box9}></View>
          <View style={styles.box10}></View>
        </ScrollView>
      </View>    
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
  },
  header: {
    flex: 1,
    flexDirection: "row",
  },
  box1: {
    width: 70,
    height: 70,
    backgroundColor: "#6AD991",
    borderRadius: 50,
    margin: 5,
    marginTop: 38,
  },
  box2: {
    width: 70,
    height: 70,
    backgroundColor: "#F2059F",
    borderRadius: 50,
    margin: 5,
    marginTop: 38,
  },
  box3: {
    width: 70,
    height: 70,
    backgroundColor: "#F20587",
    borderRadius: 50,
    margin: 5,
    marginTop: 38,
  },
  box4: {
    width: 70,
    height: 70,
    backgroundColor: "#F2AF88",
    borderRadius: 50,
    margin: 5,
    marginTop: 38,
  },
  box5: {
    width: 70,
    height: 70,
    backgroundColor: "#D97762",
    borderRadius: 50,
    margin: 5,
    marginTop: 38,
  },
  box6: {
    width: 70,
    height: 70,
    backgroundColor: "#F333B0",
    borderRadius: 50,
    margin: 5,
    marginTop: 38,
  },
  box7: {
    width: 70,
    height: 70,
    backgroundColor: "#F3339C",
    borderRadius: 50,
    margin: 5,
    marginTop: 38,
  },
  box8: {
    width: 70,
    height: 70,
    backgroundColor: "#96EAB2",
    borderRadius: 50,
    margin: 5,
    marginTop: 38,
  },
  box9: {
    width: 70,
    height: 70,
    backgroundColor: "#FFD1B5",
    borderRadius: 50,
    margin: 5,
    marginTop: 38,
  },
  box10: {
    width: 70,
    height: 70,
    backgroundColor: "#FFAC9B",
    borderRadius: 50,
    margin: 5,
    marginTop: 38,
  },
});
