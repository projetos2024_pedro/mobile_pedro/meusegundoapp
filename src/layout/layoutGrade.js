import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, ScrollView } from "react-native";

export default function LayoutGrade() {
  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <View style={styles.row}>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
      </View>
      <View style={styles.row}>
        <View style={styles.box3}></View>
        <View style={styles.box4}></View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  row: {
    flexDirection: "row",
  },
  box1: {
    width: 50,
    height: 50,
    backgroundColor: "#8747FE"
  },
  box2: {
    width: 50,
    height: 50,
    backgroundColor: "#FF5DEF"
  },
  box3: {
    width: 50,
    height: 50,
    backgroundColor: "#2D26EF"
  },
  box4: {
    width: 50,
    height: 50,
    backgroundColor: "#088FFE"
  },
});
