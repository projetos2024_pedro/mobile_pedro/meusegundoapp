import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Button } from 'react-native';
import LayoutHorizontal from './layout/layoutHorizontal';

function SegundaTela ({navigation}) {
    return (
      <View style={styles.container}>
          <Text style={styles.text} >
            Segunda tela
          </Text>
          <TouchableOpacity style={styles.button} onPress={() => navigation.goBack()}>
            <Text style={styles.textButton}>Voltar para Home</Text>
          </TouchableOpacity>
          <LayoutHorizontal></LayoutHorizontal>
        </View>
      )
    }

    export default SegundaTela;

    const styles = StyleSheet.create({
        container: {
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'space-around',
          alignItems: 'center',
        },
        text: {
           marginTop: 10,
        },
        button: {
            marginTop: 25,
            padding: 10,
            backgroundColor: "#F20544",
            borderRadius: 7
        },
        textButton: {
            color: '#FFFAFA', 
        }
      });
    