import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, TextInput, Button } from 'react-native';
import { useState, useEffect } from "react";

export default function Components() {
    const [text, setText] = useState("");
    const [numLetras, setNumLetras] = useState(0);

    function click(){
        setNumLetras(text.length)
    }


  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
        <Text style={styles.text}>Viúvas do Pelé 👻🅱{text}</Text>
        <TextInput
        style={styles.input}
        placeholder="Qual a divisão do Santos FC 👻🅱?"
        value={text}
        onChangeText={(textInput) => setText (textInput)}
        />
        <Button 
        style={styles.button}
        title="👻🅱" 
        onPress={() => {
            click();
        }}/>
        {numLetras > 0 ? <Text>{numLetras}</Text> : <Text></Text>}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: "center"
  },
  text: {
    fontWeight: 'bold', 
    fontSize: 18
  }, 
  input: {
    margin: 20,
    fontSize: 17, 
    borderColor: '#D0D9D4', 
    borderWidth: 1,
    width: "80%",
    padding: 10,
    marginVertical: 10
  },
});
