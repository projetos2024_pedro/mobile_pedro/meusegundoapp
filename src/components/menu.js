import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';

export default function Menu({navigation}) {
  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <TouchableOpacity onPress={() => navigation.navigate('TelaInicial')} style={styles.menu}>
      <Text style={styles.text}>Opção 1</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate('SegundaTela')}  style={styles.menu}>
      <Text style={styles.text}>Opção 2</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  menu: {
    padding: 10,
    backgroundColor: '#61E8DF',
    borderRadius: 7,
  },
  text: {
    color: "#FFFAFA",
    fontWeight: 'bold'
  }
});
