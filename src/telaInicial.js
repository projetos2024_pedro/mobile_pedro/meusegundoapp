import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Button } from 'react-native';


function TelaInicial ({navigation}) {
    return (
      <View style={styles.container}>
        <Text>
          Bem vindo à tela inicial!
        </Text>
      <View style={styles.row}>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
      </View>
      <View style={styles.row}>
        <View style={styles.box3}></View>
        <View style={styles.box4}></View>
      </View>
        <Button style={styles.button}  title="Ir para a próxima página" onPress={() => navigation.navigate('SegundaTela')}></Button>
      </View>
    )
  }

  export default TelaInicial;

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
    },
    row: {
        flexDirection: "row",
      },
      box1: {
        width: 50,
        height: 50,
        backgroundColor: "#8747FE",
        marginTop: 35
      },
      box2: {
        width: 50,
        height: 50,
        backgroundColor: "#FF5DEF",
        marginTop: 35
      },
      box3: {
        width: 50,
        height: 50,
        backgroundColor: "#2D26EF",
        marginBottom: 35
      },
      box4: {
        width: 50,
        height: 50,
        backgroundColor: "#088FFE",
        marginBottom: 35
      },
  });